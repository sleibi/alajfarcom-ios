//
//  WebViewController.swift
//  alajfarcom
//
//  Created by Design on 6/29/17.
//  Copyright © 2017 Tkniyati. All rights reserved.
//

import UIKit

class WebViewController: UIViewController{
    var url = ""
    @IBOutlet var webView1: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let request = NSMutableURLRequest(url: NSURL(string: url)! as URL)
        request.setValue("ios", forHTTPHeaderField:"app")
        webView1.loadRequest(request as URLRequest)
        //webView1.loadRequest(NSURLRequest(url: NSURL(string: url)! as URL) as URLRequest)
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backPressed(_ sender: UIButton) {
        if(webView1.canGoBack)
        {
            webView1.goBack()
        }
        else{
            
            dismiss(animated: true, completion: nil)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    

}
