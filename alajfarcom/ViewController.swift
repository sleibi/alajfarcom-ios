//
//  ViewController.swift
//  alajfarcom
//
//  Created by Design on 6/29/17.
//  Copyright © 2017 Tkniyati. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func snap(_ sender: UIButton) {
        let next = self.storyboard?.instantiateViewController(withIdentifier: "WebViewPage") as! WebViewController
        
        next.url = "http://alajfarcom.com/__/%D8%B3%D9%86%D8%A7%D8%A8%20%D8%A7%D9%84%D8%A3%D8%AC%D9%81%D8%B1%20%D9%84%D8%A7%D9%8A%D9%81"
        self.present(next, animated: false, completion: nil)
    }
    @IBAction func transport(_ sender: UIButton) {
        let next = self.storyboard?.instantiateViewController(withIdentifier: "WebViewPage") as! WebViewController
        
        next.url = "http://alajfarcom.com/__/%D8%AA%D9%88%D8%B5%D9%8A%D9%84%20%D9%88%D9%85%D8%B4%D8%A7%D9%88%D9%8A%D8%B1"
        self.present(next, animated: false, completion: nil)
    }
    @IBAction func family(_ sender: UIButton) {
        let next = self.storyboard?.instantiateViewController(withIdentifier: "WebViewPage") as! WebViewController
        
        next.url = "http://alajfarcom.com/__/%D8%A7%D9%84%D8%A3%D8%B3%D8%B1%20%D8%A7%D9%84%D9%85%D9%86%D8%AA%D8%AC%D8%A9"
        self.present(next, animated: false, completion: nil)
    }
    @IBAction func signin(_ sender: UIButton) {
        let next = self.storyboard?.instantiateViewController(withIdentifier: "WebViewPage") as! WebViewController

        next.url = "http://alajfarcom.com/login.php"
        self.present(next, animated: false, completion: nil)
    }
    @IBAction func search(_ sender: UIButton) {
        let next = self.storyboard?.instantiateViewController(withIdentifier: "WebViewPage") as! WebViewController
        
        next.url = "http://alajfarcom.com/advsearch.php"
        self.present(next, animated: false, completion: nil)
    }

    @IBAction func services(_ sender: UIButton) {
        let next = self.storyboard?.instantiateViewController(withIdentifier: "WebViewPage") as! WebViewController
        
        next.url = "http://alajfarcom.com/__/%D8%AE%D8%AF%D9%85%D8%A7%D8%AA"
        self.present(next, animated: false, completion: nil)
    }
    @IBAction func pets(_ sender: UIButton) {
        let next = self.storyboard?.instantiateViewController(withIdentifier: "WebViewPage") as! WebViewController
        
        next.url = "http://alajfarcom.com/__/%D9%85%D9%88%D8%A7%D8%B4%D9%8A%20%D9%88%20%D8%AD%D9%8A%D9%88%D8%A7%D9%86%D8%A7%D8%AA%20%D9%88%20%D8%B7%D9%8A%D9%88%D8%B1"
        self.present(next, animated: false, completion: nil)
    }
    @IBAction func signup(_ sender: UIButton) {
        let next = self.storyboard?.instantiateViewController(withIdentifier: "WebViewPage") as! WebViewController
        
        next.url = "http://alajfarcom.com/register.php"
        self.present(next, animated: false, completion: nil)
    }
    @IBAction func cars(_ sender: UIButton) {
        let next = self.storyboard?.instantiateViewController(withIdentifier: "WebViewPage") as! WebViewController
        
        next.url = "http://alajfarcom.com/__/%20%D8%A7%D9%84%D8%B3%D9%8A%D8%A7%D8%B1%D8%A7%D8%AA"
        self.present(next, animated: false, completion: nil)
    }
    @IBAction func devices(_ sender: UIButton) {
        let next = self.storyboard?.instantiateViewController(withIdentifier: "WebViewPage") as! WebViewController
        
        next.url = "http://alajfarcom.com/__/%D8%A7%D9%84%D8%A3%D8%AC%D9%87%D8%B2%D8%A9"
        self.present(next, animated: false, completion: nil)
    }
    @IBAction func realestate(_ sender: UIButton) {
        let next = self.storyboard?.instantiateViewController(withIdentifier: "WebViewPage") as! WebViewController
        
        next.url = "http://alajfarcom.com/__/%D8%A7%D9%84%D8%B9%D9%82%D8%A7%D8%B1%D8%A7%D8%AA"
        self.present(next, animated: false, completion: nil)
    }
}

